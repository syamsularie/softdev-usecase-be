package tests

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"softdev/usecase-be/api/models"
	"testing"

	"github.com/gorilla/mux"
	"gopkg.in/go-playground/assert.v1"
)

func TestGetListProduct(t *testing.T) {

	err := refreshProductTable()
	if err != nil {
		log.Fatal(err)
	}
	_, err = seedProducts()
	if err != nil {
		log.Fatal(err)
	}

	req, err := http.NewRequest("GET", "/product/list", nil)
	if err != nil {
		t.Errorf("this is the error: %v\n", err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(server.GetProducts)
	handler.ServeHTTP(rr, req)

	var product []models.Product
	err = json.Unmarshal(rr.Body.Bytes(), &product)
	if err != nil {
		t.Errorf("this is the error: %v\n", err)
	}

	assert.Equal(t, rr.Code, http.StatusOK)
	assert.Equal(t, len(product), 4)
}

func TestSearchProduct(t *testing.T) {

	err := refreshProductTable()
	if err != nil {
		log.Fatal(err)
	}
	_, err = seedProducts()
	if err != nil {
		log.Fatal(err)
	}

	req, err := http.NewRequest("GET", "/product/search", nil)
	if err != nil {
		t.Errorf("this is the error: %v\n", err)
	}
	req = mux.SetURLVars(req, map[string]string{"keyword": "minyak"})
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(server.SearchProductByName)
	handler.ServeHTTP(rr, req)

	var product []models.Product
	err = json.Unmarshal(rr.Body.Bytes(), &product)
	if err != nil {
		t.Errorf("this is the error: %v\n", err)
	}
	assert.Equal(t, rr.Code, http.StatusOK)
	assert.Equal(t, len(product), 2)
}
