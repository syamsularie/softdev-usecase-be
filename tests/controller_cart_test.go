package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"gopkg.in/go-playground/assert.v1"
)

func TestAddToCart(t *testing.T) {

	err := refreshAllTable()
	if err != nil {
		log.Fatal(err)
	}

	user, err := seedOneUser()
	if err != nil {
		log.Fatalf("Cannot seed user %v\n", err)
	}
	token, err := server.SignIn(user.Email, "password") //Note the password in the database is already hashed, we want unhashed
	if err != nil {
		log.Fatalf("cannot login: %v\n", err)
	}
	tokenString := fmt.Sprintf("Bearer %v", token)

	samples := []struct {
		inputJSON    string
		statusCode   int
		userId       uint32
		productId    int32
		total        int32
		tokenGiven   string
		errorMessage string
	}{
		{
			inputJSON:    `{"product_id" : 1,"total": 2}`,
			statusCode:   201,
			tokenGiven:   tokenString,
			productId:    1,
			total:        2,
			userId:       user.ID,
			errorMessage: "",
		},
		{
			inputJSON:    `{"total": 4}`,
			statusCode:   422,
			tokenGiven:   tokenString,
			errorMessage: "required Product Id",
		},
		{
			inputJSON:    `{"product_id" : 2}`,
			statusCode:   400,
			tokenGiven:   tokenString,
			errorMessage: "required Total",
		},
	}
	for _, v := range samples {

		req, err := http.NewRequest("POST", "/cart/add", bytes.NewBufferString(v.inputJSON))
		if err != nil {
			t.Errorf("this is the error: %v\n", err)
		}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(server.AddToCart)

		req.Header.Set("Authorization", v.tokenGiven)
		handler.ServeHTTP(rr, req)

		responseMap := make(map[string]interface{})
		err = json.Unmarshal(rr.Body.Bytes(), &responseMap)
		if err != nil {
			fmt.Printf("Cannot convert to json: %v", err)
		}
		assert.Equal(t, rr.Code, v.statusCode)
		if v.statusCode == 201 {
			assert.Equal(t, rr.Code, http.StatusCreated)
		}

		if v.statusCode == 401 || v.statusCode == 422 || v.statusCode == 500 && v.errorMessage != "" {
			assert.Equal(t, responseMap["error"], v.errorMessage)
		}
	}
}
