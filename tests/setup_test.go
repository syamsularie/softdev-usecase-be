package tests

import (
	"fmt"
	"log"
	"os"
	"testing"

	"softdev/usecase-be/api/controllers"
	"softdev/usecase-be/api/models"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
)

var server = controllers.Server{}
var userInstance = models.User{}
var productInstance = models.Product{}

//var postInstance = models.Post{}

func TestMain(m *testing.M) {
	//var err error
	err := godotenv.Load(os.ExpandEnv("./../.env"))
	if err != nil {
		log.Fatalf("Error getting env %v\n", err)
	}
	Database()

	os.Exit(m.Run())

}

func Database() {

	var err error

	TestDbDriver := os.Getenv("TEST_DB_DRIVER")

	if TestDbDriver == "mysql" {
		DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("TEST_DB_USER"), os.Getenv("TEST_DB_PASSWORD"), os.Getenv("TEST_DB_HOST"), os.Getenv("TEST_DB_PORT"), os.Getenv("TEST_DB_NAME"))
		server.DB, err = gorm.Open(TestDbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestDbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", TestDbDriver)
		}
	}
	if TestDbDriver == "postgres" {
		DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", os.Getenv("TEST_DB_HOST"), os.Getenv("TEST_DB_PORT"), os.Getenv("TEST_DB_USER"), os.Getenv("TEST_DB_NAME"), os.Getenv("TEST_DB_PASSWORD"))
		server.DB, err = gorm.Open(TestDbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestDbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", TestDbDriver)
		}
	}
}

func refreshUserTable() error {
	err := server.DB.DropTableIfExists(&models.User{}).Error
	if err != nil {
		return err
	}
	err = server.DB.AutoMigrate(&models.User{}).Error
	if err != nil {
		return err
	}
	log.Printf("Successfully refreshed table")
	return nil
}

func seedOneUser() (models.User, error) {

	err := refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}

	user := models.User{
		Nickname: "Syamsul Bachri",
		Email:    "syamsul@gmail.com",
		Password: "password",
	}

	err = server.DB.Model(&models.User{}).Create(&user).Error
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}

func seedUsers() ([]models.User, error) {

	var err error
	if err != nil {
		return nil, err
	}
	users := []models.User{
		models.User{
			Nickname: "Syamsul Bachri",
			Email:    "syamsul@gmail.com",
			Password: "password",
		},
		models.User{
			Nickname: "Bachri Syamsul",
			Email:    "Bachri@gmail.com",
			Password: "password",
		},
	}

	for i, _ := range users {
		err := server.DB.Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			return []models.User{}, err
		}
	}
	return users, nil
}

func refreshProductTable() error {

	err := server.DB.DropTableIfExists(&models.Product{}).Error
	if err != nil {
		return err
	}
	err = server.DB.AutoMigrate(&models.Product{}).Error
	if err != nil {
		return err
	}
	log.Printf("Successfully refreshed tables")
	return nil
}

func refreshAllTable() error {

	err := server.DB.DropTableIfExists(&models.User{}, &models.Product{}, &models.Cart{}, &models.Transaction{}, &models.Promo{}).Error
	if err != nil {
		return err
	}
	err = server.DB.AutoMigrate(&models.User{}, &models.Product{}, &models.Cart{}, &models.Transaction{}, &models.Promo{}).Error
	if err != nil {
		return err
	}
	log.Printf("Successfully refreshed tables")
	return nil
}

func seedProducts() ([]models.Product, error) {

	var err error

	var products = []models.Product{
		models.Product{
			ProductName: "Minyak Goreng",
			Description: "Minyak untuk menggoreng",
			Category:    "Minyak",
			Price:       5000,
			Quantity:    100,
		},
		{
			ProductName: "Minyak Gosok",
			Description: "Minyak untuk menggosok",
			Category:    "Minyak",
			Price:       1000,
			Quantity:    70,
		},
		{
			ProductName: "Roti Tawa",
			Description: "Roti yang tawar",
			Category:    "Roti",
			Price:       11000,
			Quantity:    40,
		},
		{
			ProductName: "Shampo",
			Description: "Shampo Rambut",
			Category:    "Sabun",
			Price:       10000,
			Quantity:    121,
		},
	}

	for i, _ := range products {
		err = server.DB.Model(&models.Product{}).Create(&products[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
	}
	return products, nil
}

func seedUserProductsCartTransactionPromo() ([]models.User, []models.Product, []models.Cart, []models.Transaction, []models.Promo, error) {

	var err error

	var users = []models.User{
		models.User{
			Nickname: "Syamsul Bachri",
			Email:    "syamsul@gmail.com",
			Password: "password",
		},
		models.User{
			Nickname: "Bachri Syamsul",
			Email:    "bachri@gmail.com",
			Password: "password",
		},
	}

	var products = []models.Product{
		models.Product{
			ProductName: "Minyak Goreng",
			Description: "Minyak untuk menggoreng",
			Category:    "Minyak",
			Price:       5000,
			Quantity:    100,
		},
		{
			ProductName: "Minyak Gosok",
			Description: "Minyak untuk menggosok",
			Category:    "Minyak",
			Price:       1000,
			Quantity:    70,
		},
		{
			ProductName: "Roti Tawa",
			Description: "Roti yang tawar",
			Category:    "Roti",
			Price:       11000,
			Quantity:    40,
		},
		{
			ProductName: "Shampo",
			Description: "Shampo Rambut",
			Category:    "Sabun",
			Price:       10000,
			Quantity:    121,
		},
	}

	var transaction = []models.Transaction{
		models.Transaction{
			TransactionId: "e7ee4de9-09ed-4566-9a7b-d76bf185fda4",
			ProductId:     1,
			UserId:        1,
			Total:         5,
			Price:         20000,
		},
		{
			TransactionId: "e7ee4de9-09ed-4566-9a7b-d76bf185fda4",
			ProductId:     2,
			UserId:        1,
			Total:         8,
			Price:         6000,
		},
		{
			TransactionId: "e7ee4de9-09ed-4566-9a7b-d76bf185fda4",
			ProductId:     4,
			UserId:        1,
			Total:         3,
			Price:         30000,
		},
	}

	var cart = []models.Cart{
		models.Cart{
			CartId:    1,
			ProductId: 1,
			UserId:    1,
			Total:     5,
		},
		{
			CartId:    2,
			ProductId: 2,
			UserId:    1,
			Total:     8,
		},
	}

	var promo = []models.Promo{
		models.Promo{
			PromoId:       1,
			ProductId:     1,
			Description:   "Beli 3 Gratis 1",
			PromoItemBuy:  3,
			PromoItemFree: 1,
		},
		{
			PromoId:       2,
			ProductId:     2,
			Description:   "Beli 5 Gratis 2",
			PromoItemBuy:  5,
			PromoItemFree: 2,
		},
	}

	for i, _ := range users {
		err = server.DB.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
	}

	for i, _ := range products {
		err = server.DB.Debug().Model(&models.Product{}).Create(&products[i]).Error
		if err != nil {
			log.Fatalf("cannot seed products table: %v", err)
		}
	}

	for i, _ := range cart {
		err = server.DB.Debug().Model(&models.Cart{}).Create(&cart[i]).Error
		if err != nil {
			log.Fatalf("cannot seed products table: %v", err)
		}
	}

	for i, _ := range promo {
		err = server.DB.Debug().Model(&models.Promo{}).Create(&promo[i]).Error
		if err != nil {
			log.Fatalf("cannot seed products table: %v", err)
		}
	}

	for i, _ := range transaction {
		err = server.DB.Debug().Model(&models.Transaction{}).Create(&transaction[i]).Error
		if err != nil {
			log.Fatalf("cannot seed products table: %v", err)
		}
	}
	return users, products, cart, transaction, promo, nil
}
