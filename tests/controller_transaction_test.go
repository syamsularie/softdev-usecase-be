package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"gopkg.in/go-playground/assert.v1"
)

func TestCheckOut(t *testing.T) {
	err := refreshAllTable()
	if err != nil {
		log.Fatal(err)
	}

	user, _, _, _, _, err := seedUserProductsCartTransactionPromo()

	if err != nil {
		log.Fatalf("Cannot seed user %v\n", err)
	}
	token, err := server.SignIn(user[0].Email, "password") //Note the password in the database is already hashed, we want unhashed
	if err != nil {
		log.Fatalf("cannot login: %v\n", err)
	}
	tokenString := fmt.Sprintf("Bearer %v", token)
	samples := []struct {
		inputJSON    string
		statusCode   int
		userId       uint32
		status_desc  string
		tokenGiven   string
		errorMessage string
	}{
		{
			inputJSON:    `{"cart":[{"cart_id": 1},{"cart_id": 2}]}`,
			statusCode:   201,
			tokenGiven:   tokenString,
			userId:       user[0].ID,
			status_desc:  "Checkout Success",
			errorMessage: "",
		},
		{
			inputJSON:  `{"cart":[{"cart_id": 1},{"cart_id": 2}]}`,
			statusCode: 404,
			tokenGiven: "Wrong Token",
		},
	}

	for _, v := range samples {

		req, err := http.NewRequest("POST", "/transaction/checkOut", bytes.NewBufferString(v.inputJSON))
		if err != nil {
			t.Errorf("this is the error: %v\n", err)
		}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(server.CheckOut)

		req.Header.Set("Authorization", v.tokenGiven)
		handler.ServeHTTP(rr, req)

		responseMap := make(map[string]interface{})
		err = json.Unmarshal(rr.Body.Bytes(), &responseMap)
		if err != nil {
			fmt.Printf("Cannot convert to json: %v", err)
		}
		fmt.Println(responseMap)
		assert.Equal(t, rr.Code, v.statusCode)
		if v.statusCode == 201 {
			assert.Equal(t, rr.Code, http.StatusCreated)
		}

		if v.statusCode == 401 || v.statusCode == 422 || v.statusCode == 500 && v.errorMessage != "" {
			assert.Equal(t, rr.Code, v.statusCode)
		}
	}
}

func TestCheckTotalPrice(t *testing.T) {
	err := refreshAllTable()
	if err != nil {
		log.Fatal(err)
	}

	user, _, _, _, _, err := seedUserProductsCartTransactionPromo()

	if err != nil {
		log.Fatalf("Cannot seed user %v\n", err)
	}
	token, err := server.SignIn(user[0].Email, "password") //Note the password in the database is already hashed, we want unhashed
	if err != nil {
		log.Fatalf("cannot login: %v\n", err)
	}
	tokenString := fmt.Sprintf("Bearer %v", token)
	samples := []struct {
		inputJSON             string
		statusCode            int
		userId                uint32
		transactionID         string
		totalPriceTransaction int32
		tokenGiven            string
	}{
		{
			inputJSON:             `{"transaction_id": "e7ee4de9-09ed-4566-9a7b-d76bf185fda4"}`,
			statusCode:            201,
			transactionID:         "e7ee4de9-09ed-4566-9a7b-d76bf185fda4",
			tokenGiven:            tokenString,
			totalPriceTransaction: 56000,
			userId:                user[0].ID,
		},
	}

	for _, v := range samples {

		req, err := http.NewRequest("POST", "/transaction/checkTotalPrice", bytes.NewBufferString(v.inputJSON))
		if err != nil {
			t.Errorf("this is the error: %v\n", err)
		}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(server.CheckTotalPrice)

		req.Header.Set("Authorization", v.tokenGiven)
		handler.ServeHTTP(rr, req)

		responseMap := make(map[string]interface{})
		err = json.Unmarshal(rr.Body.Bytes(), &responseMap)
		if err != nil {
			fmt.Printf("Cannot convert to json: %v", err)
		}
		assert.Equal(t, rr.Code, v.statusCode)
		if v.statusCode == 201 {
			assert.Equal(t, responseMap["transaction_id"], v.transactionID)
			assert.Equal(t, responseMap["total_price_trasaction"], float64(v.totalPriceTransaction))
		}

		if v.statusCode == 401 || v.statusCode == 422 || v.statusCode == 500 {
			assert.Equal(t, rr.Code, v.statusCode)
		}
	}
}
