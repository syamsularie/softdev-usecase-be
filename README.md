# Softdev-Usecase-BE-Shopping-Order

This is simple REST API Shopping Order Transaction

## Feature :

- Signup (Register)
- Edit his account
- Shutdown (Delete his account)
- View List Product
- Search Product By Keyword
- Add Product to Cart
- Remove Product from Cart
- Checkout Cart
- Check Total Transaction

## This API will be built with:

- Go
- GORM (A Golang ORM)
- JWT
- Mysql
- Gorilla Mux (For HTTP routing and URL matcher)

## Step 1. Basic Setup
Create the directory the project will live in.
```
mkdir fullstack
```

Download Repository

We will be using third party packages in this application. If you have never installed them before, you can run the following commands:


```
go get github.com/badoux/checkmail
go get github.com/jinzhu/gorm
go get golang.org/x/crypto/bcrypt
go get github.com/dgrijalva/jwt-go
go get github.com/gorilla/mux
go get github.com/jinzhu/gorm/dialects/mysql" 
go get github.com/joho/godotenv
go get gopkg.in/go-playground/assert.v1
go get github.com/gofrs/uuid
```



At this point this is the structure we have:
```
fullstack
├── api
│   └── auth
│   │   └── token.go
│   ├── controllers
│   │   ├── base.go 
│   │   ├── cart_controller.go
│   │   ├── home_controller.go
│   │   ├── login_controller.go
│   │   ├── product_controller.go  
│   │   ├── transaction_controller.go  
│   │   ├── user_controller.go  
│   │   └── routes.go
│   ├── middlewares 
│   │     └── middlewares.go
│   ├── models
│   │     ├── Cart.go
│   │     ├── Product.go
│   │     ├── Promo.go
│   │     ├── Transaction.go
│   │     └── User.go
│   ├── responses
│   │     └── json.go
│   ├── seed
│   │     └── seeder.go
│   ├── utils
│   │     └── formaterror
│   │         └── formaterror.go
│   │     
│   │     
│   └── server.go
├── tests
├── .env
├── go.mod
├── go.sum
└── main.go
```

## Step 2. Env Details

Let’s settle the database connection details now.
Open the fullstack folder in your favorite editor (VSCode, Goland, etc). Then open the .env file
Ensure that your database(s) are created and you have entered relevant data in the .env file.

```
# Mysql Live
#DB_HOST=fullstack-mysql                       
DB_HOST=127.0.0.1                           # when running the app without docker 
DB_DRIVER=mysql 
API_SECRET=98hbun98h                          # Used for creating a JWT. Can be anything 
DB_USER=root
DB_PASSWORD=YourPassword
DB_NAME=fullstack_api
DB_PORT=3306 

# Mysql Test
#TEST_DB_HOST=mysql_test                        
TEST_DB_HOST=127.0.0.1                       # when running the app without docker 
TEST_DB_DRIVER=mysql
TEST_API_SECRET=98hbun98h
TEST_DB_USER=root
TEST_DB_PASSWORD=YourPassword
TEST_DB_NAME=fullstack_api_test
TEST_DB_PORT=3306
```

## Step 3 Start Application
Now without further ado, you can run the app:

```
go run main.go
```
This will start up the app and run migrations.

## Step 4 Testing the Endpoints in Postman
You can use Postman or your favorite testing tool, then in the next step, we will write test cases.

### Postman Collection
```
https://www.getpostman.com/collections/7c0d348816b439ccd4ed
```

Lets test endpoints:

### a. Login (/login)
![](images/login.png)
### b. Get Product List (/product/list)
![](images/list_product.png)
### c. Search Product By Name (/product/search/{keyword})
![](images/search_by_keyword.png)
### d. Add Product to Cart (/cart/add)
![](images/add_to_cart.png)
### e. Remove From Cart (/cart/remove)
![](images/remove_cart.png)
### f. Checkout Shoping (/transaction/checkOut)
![](images/checkout.png)
### g. Check Detail and Total Transaction (/transaction/checkTotalPrice)
![](images/cek_total_price.png)

## Step 5 Running Test Cases For Endpoints

To run the test suite in the modeltests package, make sure in your terminal, you are in the path: tests.
Then run:

```
go test -v
```


## How To Run Apps With Docker

### Requirement
- Firt Download Repo
- You should also have docker installed on your machine.

##Step 1: Building a Docker Image for the application.
We do this by creating a Dockerfile from the project root directory:

With the content:

```
# Start from golang base image
FROM golang:alpine as builder

# ENV GO111MODULE=on

# Add Maintainer info
LABEL maintainer="Syamsul Bachri <syams.arie@gmail.com>"

# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git

# Set the current working directory inside the container 
WORKDIR /app

# Copy go mod and sum files 
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and the go.sum files are not changed 
RUN go mod download 

# Copy the source from the current directory to the working Directory inside the container 
COPY . .

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

# Start a new stage from scratch
FROM alpine:latest
RUN apk --no-cache add ca-certificates

WORKDIR /root/

# Copy the Pre-built binary file from the previous stage. Observe we also copied the .env file
COPY --from=builder /app/main .
COPY --from=builder /app/.env .       

# Expose port 8080 to the outside world
EXPOSE 8080

#Command to run the executable
CMD ["./main"]
```


## Step 2: Using docker-compose to build the application services.

This application needs more than one container/service. We will need a container for:
- The application container/service for the API
- Database container/service
- Database admin tools such as phpmyadmin (mysql)

Having all these in mind, we will be using docker compose to define all the services(containers) our application need.
From the directory root create a docker-compose.yml file:


```
version: '3'
services:
  app:
    container_name: full_app
    build: .
    ports: 
      - 8080:9001 
    volumes:
      - api:/usr/src/app/
    depends_on:
      - fullstack-mysql          
    networks:
      - fullstack


  fullstack-mysql:
    platform: linux/x86_64  #Comment this if you are not using M1 silicon mac
    image: mysql:5.7
    container_name: full_db_mysql
    ports: 
      - 3306:3306
    env_file: ./.env
    environment: 
      - MYSQL_ROOT_HOST=${DB_HOST} 
      - MYSQL_USER=${DB_USER}
      - MYSQL_PASSWORD=${DB_PASSWORD}
      - MYSQL_DATABASE=${DB_NAME}
      - MYSQL_ROOT_PASSWORD=${DB_PASSWORD}
    volumes:
      - database_mysql:/var/lib/mysql
    networks:
      - fullstack
  
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    container_name: phpmyadmin_container
    depends_on:
      - fullstack-mysql
    environment:
      - PMA_HOST=fullstack-mysql # Note the "fullstack-mysql". Must be the name of the what you used as the mysql service.
      - PMA_USER=${DB_USER}
      - PMA_PORT=${DB_PORT}
      - PMA_PASSWORD=${DB_PASSWORD}
    ports:
      - 9090:80
    restart: always
    networks:
      - fullstack


volumes:
  api:
  database_mysql:                  

# Networks to be created to facilitate communication between containers
networks:
  fullstack:
    driver: bridge
```

## Step 3: Editing your .env values

```
# Mysql Live
DB_HOST=fullstack-mysql                       
#DB_HOST=127.0.0.1                           # when running the app without docker 
DB_DRIVER=mysql 
API_SECRET=98hbun98h                          # Used for creating a JWT. Can be anything 
DB_USER=root
DB_PASSWORD=syamsulbac
DB_NAME=fullstack_api
DB_PORT=3306 
# Mysql Test
TEST_DB_HOST=mysql_test                        
#TEST_DB_HOST=127.0.0.1                       # when running the app without docker 
TEST_DB_DRIVER=mysql
TEST_API_SECRET=98hbun98h
TEST_DB_USER=root
TEST_DB_PASSWORD=syamsulbac
TEST_DB_NAME=fullstack_api_test
TEST_DB_PORT=3306
```

## Step 4: Running the application

We are now ready to create and start all the containers/services listed in the docker-compose.
Run:

```
docker-compose up
```

Now that you have confirmed that everything works, you can stop the app from running using:
```
docker-compose down 
```
Then run the app again in the background using:
```
docker-compose up -d
```
The above command will display no log in the terminal as the app is now running in the background.

Successful Build
When the build is successful, happily go to your browser or postman and visit:
```
http://localhost:8080
```

## Link Repo Image Docker
```
https://hub.docker.com/repository/docker/syamsularie/softdev-usecase-be_app
```


### Further Development Suggestion
This Application Is not complete yet, you can add more featur such as :

- Payment Gateway Method
- Recomended Product
- Whislist Product
- Favorite Category
- Etc.