module softdev/usecase-be

go 1.17

require (
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.4.0
)

require (
	github.com/badoux/checkmail v1.2.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.3 // indirect
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064
	gopkg.in/go-playground/assert.v1 v1.2.1
	gorm.io/gorm v1.22.3 // indirect
)
