package controllers

import (
	"net/http"

	"softdev/usecase-be/api/responses"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Welcome To Usecase API")

}
