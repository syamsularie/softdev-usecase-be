package controllers

import (
	"encoding/json"
	"io/ioutil"
	"math"
	"net/http"
	"softdev/usecase-be/api/models"
	"softdev/usecase-be/api/responses"
	"softdev/usecase-be/api/utils/formaterror"

	"github.com/gofrs/uuid"
)

type cartID struct {
	CartId int32 `json:"cart_id"`
}

type checkOut struct {
	Cart []cartID `json:"cart"`
}

type transactionID struct {
	TransactionID string `json:"transaction_id"`
}

type detailTransaction struct {
	ProductName   string  `json:"product_name"`
	Promo         string  `json:"promo"`
	PricePerPiece float32 `json:"price_per_piece"`
	Quantity      int32   `json:"quantity"`
	TotalPrice    float32 `json:"total_price"`
}

type responseTotalTransaction struct {
	TransactionID         string              `json:"transaction_id"`
	TotalPriceTransaction float32             `json:"total_price_trasaction"`
	DetailTransaction     []detailTransaction `json:"detail_transaction"`
}

func (server *Server) CheckOut(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}

	var cartID checkOut
	err = json.Unmarshal(body, &cartID)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	carts := models.Cart{}
	products := models.Product{}
	promos := models.Promo{}

	uuid := uuid.Must(uuid.NewV4())

	for _, v := range cartID.Cart {
		// Find Carts to be checkedout
		cart, err := carts.FindCartByID(server.DB, v.CartId)
		if err != nil {
			responses.ERROR(w, http.StatusNotFound, err)
			return
		}
		// Find Products data
		product, err := products.FindProductByID(server.DB, cart.ProductId)
		if err != nil {
			responses.ERROR(w, http.StatusNotFound, err)
			return
		}
		// Find Promo
		promo, _ := promos.FindPromoByProductID(server.DB, cart.ProductId)

		// Define promo item free
		var harga float64
		if promo.PromoItemBuy != 0 {
			harga = float64(product.Price) * (float64(cart.Total) - (math.Floor(float64(cart.Total)/float64(promo.PromoItemBuy)) * float64(promo.PromoItemFree)))
		} else {
			harga = float64(product.Price) * float64(cart.Total)
		}

		err = models.InsertTransaction(server.DB, models.Transaction{
			TransactionId: uuid.String(),
			ProductId:     cart.ProductId,
			UserId:        cart.UserId,
			Total:         cart.Total,
			Price:         float32(harga),
			PaymentStatus: "Done",
		})
		if err != nil {
			formattedError := formaterror.FormatError(err.Error())
			responses.ERROR(w, http.StatusInternalServerError, formattedError)
			return
		}

		// Update Quantity Product
		product.Quantity = product.Quantity - cart.Total
		err = product.UpdateQuantity(server.DB, cart.ProductId)
		if err != nil {
			formattedError := formaterror.FormatError(err.Error())
			responses.ERROR(w, http.StatusInternalServerError, formattedError)
			return
		}

		// Remove from cart table
		err = cart.DeleteCart(server.DB, cart.CartId)
		if err != nil {
			formattedError := formaterror.FormatError(err.Error())
			responses.ERROR(w, http.StatusInternalServerError, formattedError)
			return
		}
	}

	response := map[string]interface{}{
		"status_code": "200",
		"status_desc": "Checkout Success",
	}
	responses.JSON(w, http.StatusCreated, response)
}

func (server *Server) CheckTotalPrice(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}

	var transactionId transactionID
	err = json.Unmarshal(body, &transactionId)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	transaction := models.Transaction{}
	//Find Transaction data
	transactions, err := transaction.FindTransactionByID(server.DB, transactionId.TransactionID)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	var responseTotalPrice responseTotalTransaction
	responseTotalPrice.TransactionID = transactionId.TransactionID
	responseTotalPrice.TotalPriceTransaction = 100000

	var totalPriceTransaction float32

	products := models.Product{}
	promos := models.Promo{}
	for _, v := range *transactions {
		// Find Products data
		product, err := products.FindProductByID(server.DB, v.ProductId)
		if err != nil {
			responses.ERROR(w, http.StatusNotFound, err)
			return
		}

		// Find Promo
		promo, _ := promos.FindPromoByProductID(server.DB, v.ProductId)
		var promoDescription string
		if promo.PromoItemBuy != 0 {
			promoDescription = promo.Description
		}
		totalPriceTransaction += v.Price

		newStruct := &detailTransaction{
			ProductName:   product.ProductName,
			Promo:         promoDescription,
			PricePerPiece: product.Price,
			Quantity:      v.Total,
			TotalPrice:    v.Price,
		}
		responseTotalPrice.DetailTransaction = append(responseTotalPrice.DetailTransaction, *newStruct)

	}
	responseTotalPrice.TotalPriceTransaction = totalPriceTransaction
	responses.JSON(w, http.StatusCreated, responseTotalPrice)
}
