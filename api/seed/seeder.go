package seed

import (
	"log"

	"softdev/usecase-be/api/models"

	"github.com/jinzhu/gorm"
)

var users = []models.User{
	models.User{
		Nickname: "Syamsul Bachri",
		Email:    "syamsul@gmail.com",
		Password: "password",
	},
	models.User{
		Nickname: "Bachri Syamsul",
		Email:    "bachri@gmail.com",
		Password: "password",
	},
}

var products = []models.Product{
	models.Product{
		ProductName: "Minyak Goreng",
		Description: "Minyak untuk menggoreng",
		Category:    "Minyak",
		Price:       5000,
		Quantity:    100,
	},
	{
		ProductName: "Minyak Gosok",
		Description: "Minyak untuk menggosok",
		Category:    "Minyak",
		Price:       1000,
		Quantity:    70,
	},
	{
		ProductName: "Roti Tawa",
		Description: "Roti yang tawar",
		Category:    "Roti",
		Price:       11000,
		Quantity:    40,
	},
	{
		ProductName: "Shampo",
		Description: "Shampo Rambut",
		Category:    "Sabun",
		Price:       10000,
		Quantity:    121,
	},
}

var transaction = []models.Transaction{
	models.Transaction{
		TransactionId: "e7ee4de9-09ed-4566-9a7b-d76bf185fda4",
		ProductId:     1,
		UserId:        1,
		Total:         5,
		Price:         20000,
	},
	{
		TransactionId: "e7ee4de9-09ed-4566-9a7b-d76bf185fda4",
		ProductId:     2,
		UserId:        1,
		Total:         8,
		Price:         6000,
	},
	{
		TransactionId: "e7ee4de9-09ed-4566-9a7b-d76bf185fda4",
		ProductId:     4,
		UserId:        1,
		Total:         3,
		Price:         30000,
	},
}

var cart = []models.Cart{
	models.Cart{
		CartId:    1,
		ProductId: 1,
		UserId:    1,
		Total:     5,
	},
	{
		CartId:    2,
		ProductId: 2,
		UserId:    1,
		Total:     8,
	},
}

var promo = []models.Promo{
	models.Promo{
		PromoId:       1,
		ProductId:     1,
		Description:   "Beli 3 Gratis 1",
		PromoItemBuy:  3,
		PromoItemFree: 1,
	},
	{
		PromoId:       2,
		ProductId:     2,
		Description:   "Beli 5 Gratis 2",
		PromoItemBuy:  5,
		PromoItemFree: 2,
	},
}

func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.User{}, &models.Product{}, &models.Cart{}, &models.Promo{}, &models.Transaction{}).Error
	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}
	err = db.Debug().AutoMigrate(&models.User{}, &models.Product{}, &models.Cart{}, &models.Promo{}, &models.Transaction{}).Error
	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	for i, _ := range users {
		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
	}

	for i, _ := range products {
		err = db.Debug().Model(&models.Product{}).Create(&products[i]).Error
		if err != nil {
			log.Fatalf("cannot seed products table: %v", err)
		}
	}

	for i, _ := range cart {
		err = db.Debug().Model(&models.Cart{}).Create(&cart[i]).Error
		if err != nil {
			log.Fatalf("cannot seed products table: %v", err)
		}
	}

	for i, _ := range promo {
		err = db.Debug().Model(&models.Promo{}).Create(&promo[i]).Error
		if err != nil {
			log.Fatalf("cannot seed products table: %v", err)
		}
	}

	for i, _ := range transaction {
		err = db.Debug().Model(&models.Transaction{}).Create(&transaction[i]).Error
		if err != nil {
			log.Fatalf("cannot seed products table: %v", err)
		}
	}
}
