package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

type Promo struct {
	PromoId       int32     `gorm:"primary_key;auto_increment" json:"promo_id"`
	ProductId     int32     `gorm:"not null;" json:"product_id"`
	Description   string    `gorm:"size:255;not null;" json:"description"`
	PromoItemBuy  int32     `gorm:"not null;" json:"promo_item_buy"`
	PromoItemFree int32     `gorm:"not null;" json:"promo_item_free"`
	CreatedAt     time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt     time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (p *Promo) FindPromoByProductID(db *gorm.DB, keyword int32) (*Promo, error) {
	var err error
	promo := Promo{}
	err = db.Debug().Model(Promo{}).Where("product_id = ?", keyword).Find(&promo).Error
	if err != nil {
		return &Promo{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &Promo{}, errors.New("Promo Not Found")
	}
	return &promo, nil
}
